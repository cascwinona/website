require('dotenv').config();

const gulp = require('gulp');
const nunjucksRender = require('gulp-nunjucks-render');
const less = require('gulp-less');
const path = require('path');
const cleanCSS = require('gulp-clean-css');
const concat = require('gulp-concat');
const del = require('del');
const fse = require('fs-extra');
const data = require('gulp-data');
const replace = require('gulp-string-replace');

gulp.task('nunjucks', function () {
    return gulp.src('app/pages/**/*.+(html|njk)')
        .pipe(data(function () {
            return JSON.parse(fse.readFileSync('./app/data/mass-times.json'));
        }))
        .pipe(data(function () {
            return JSON.parse(fse.readFileSync('./app/data/confession-times.json'));
        }))
        .pipe(data(function () {
            return JSON.parse(fse.readFileSync('./app/data/staff.json'));
        }))
        .pipe(nunjucksRender({
            path: ['app/templates']
        }))
        // output files in app folder
        .pipe(gulp.dest('public'));
});


gulp.task('less', function () {
    return gulp.src('app/styles/*.less')
        .pipe(less({
            paths: [path.join(__dirname, 'less', 'includes')]
        }))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(concat('style.min.css'))
        .pipe(gulp.dest('./public/css'));
});

gulp.task('scripts', function () {
    return gulp.src(
        [
            'node_modules/jquery/dist/jquery.min.js',
            'node_modules/bootstrap/dist/js/bootstrap.min.js',
	    'node_modules/romcal/dist/romcal.bundle.min.js'
        ])
        .pipe(concat('scripts.min.js'))
        .pipe(gulp.dest('./public/js'));
});

gulp.task('copy', function () {
    return fse.copy('./app/static/', './public')
        .then(() => gulp.src('./app/pages/**/*.+(png|pdf|jpg|bmp|jpeg|gif|svg)')
            .pipe(gulp.dest('./public'))
        );
});

gulp.task('clean', function () {
    return del([
        'public/**/*',
    ]);
});

gulp.task('replace-key', function () {
    const options = {logs: {enabled: false}};
    return gulp.src(['./public/index.html'])
        .pipe(replace('@@googleAPIKey', process.env.BLOGGER_API_KEY, options))
        .pipe(gulp.dest(function (file) {
            return file.base;
        }));
});

gulp.task('build', gulp.series('less', 'nunjucks', 'scripts', 'copy', 'replace-key'));
